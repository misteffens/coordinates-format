//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include <DMS.h>
#include <Decimal.h>
#include <FL95.h>
#include <FPL.h>
#include <ICAO.h>
#include <NOTAM.h>
#include <gtest/gtest.h>

#include <DegreesOutOfRange.h>
#include <LatitudeOutOfRange.h>
#include <LongitudeOutOfRange.h>
#include <MinutesOutOfRange.h>
#include <SecondsOutOfRange.h>

// Test name coordinate format: [NS]ddmmss[EW]dddmmss.

TEST(FromICAO, N000000E0000000toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000N0000000E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromICAO, S000000W0000000toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000S0000000W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromICAO, N003600E0003600toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600N0003600E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.600000, 0.600000");
}

TEST(FromICAO, S003600W0003600toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600S0003600W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-0.600000, -0.600000");
}

TEST(FromICAO, N895959E1795959toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959N1795959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "89.999722, 179.999722");
}

TEST(FromICAO, S895959W1795959toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959S1795959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-89.999722, -179.999722");
}

TEST(FromICAO, N8959E17959toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959N17959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "89.983333, 179.983333");
}

TEST(FromICAO, S8959W17959toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959S17959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-89.983333, -179.983333");
}

TEST(FromICAO, N90E180toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("90N180E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "90.000000, 180.000000");
}

TEST(FromICAO, S90W180toDecimal)
{
	std::optional<Coordinates::Values> from{ICAO::from("90S180W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-90.000000, -180.000000");
}

TEST(FromICAO, N000000E0000000toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000N0000000E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromICAO, S000000W0000000toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000S0000000W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromICAO, N003600E0003600toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600N0003600E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''N 0°36'0.00''E");
}

TEST(FromICAO, S003600W0003600toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600S0003600W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''S 0°36'0.00''W");
}

TEST(FromICAO, N895959E1795959toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959N1795959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.00''N 179°59'59.00''E");
}

TEST(FromICAO, S895959W1795959toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959S1795959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.00''S 179°59'59.00''W");
}

TEST(FromICAO, N8959E17959toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959N17959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'0.00''N 179°59'0.00''E");
}

TEST(FromICAO, S8959W17959toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959S17959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'0.00''S 179°59'0.00''W");
}

TEST(FromICAO, N90E180toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("90N180E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "90°0'0.00''N 180°0'0.00''E");
}

TEST(FromICAO, S90W180toDMS)
{
	std::optional<Coordinates::Values> from{ICAO::from("90S180W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "90°0'0.00''S 180°0'0.00''W");
}

TEST(FromICAO, N000000E0000000toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000N0000000E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromICAO, S000000W0000000toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000S0000000W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromICAO, N003600E0003600toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600N0003600E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 36.0 E 000 36.0");
}

TEST(FromICAO, S003600W0003600toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600S0003600W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 00 36.0 W 000 36.0");
}

TEST(FromICAO, N895954E1795954toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("895954N1795954E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 89 59.9 E 179 59.9");
}

TEST(FromICAO, S895954W1795954toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("895954S1795954W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 89 59.9 W 179 59.9");
}

TEST(FromICAO, N895959E1795959toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959N1795959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 90 00.0 E 180 00.0");
}

TEST(FromICAO, S895959W1795959toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959S1795959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 90 00.0 W 180 00.0");
}

TEST(FromICAO, N8959E17959toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959N17959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 89 59.0 E 179 59.0");
}

TEST(FromICAO, S8959W17959toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959S17959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 89 59.0 W 179 59.0");
}

TEST(FromICAO, N90E180toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("90N180E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 90 00.0 E 180 00.0");
}

TEST(FromICAO, S90W180toFL95)
{
	std::optional<Coordinates::Values> from{ICAO::from("90S180W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 90 00.0 W 180 00.0");
}

TEST(FromICAO, N000000E0000000toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000N0000000E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromICAO, S000000W0000000toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000S0000000W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromICAO, N003600E0003600toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600N0003600E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036N00036E");
}

TEST(FromICAO, S003600W0003600toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600S0003600W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036S00036W");
}

TEST(FromICAO, N895959E1795959toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959N1795959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000N18000E");
}

TEST(FromICAO, S895959W1795959toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959S1795959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000S18000W");
}

TEST(FromICAO, N8959E17959toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959N17959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "8959N17959E");
}

TEST(FromICAO, S8959W17959toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959S17959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "8959S17959W");
}

TEST(FromICAO, N90E180toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("90N180E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000N18000E");
}

TEST(FromICAO, S90W180toFPL)
{
	std::optional<Coordinates::Values> from{ICAO::from("90S180W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000S18000W");
}

TEST(FromICAO, N000000E0000000toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000N0000000E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromICAO, S000000W0000000toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("000000S0000000W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromICAO, N003600E0003600toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600N0003600E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600N 0003600E");
}

TEST(FromICAO, S003600W0003600toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("003600S0003600W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600S 0003600W");
}

TEST(FromICAO, N895959E1795959toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959N1795959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895959N 1795959E");
}

TEST(FromICAO, S895959W1795959toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("895959S1795959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895959S 1795959W");
}

TEST(FromICAO, N8959E17959toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959N17959E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895900N 1795900E");
}

TEST(FromICAO, S8959W17959toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("8959S17959W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895900S 1795900W");
}

TEST(FromICAO, N90E180toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("90N180E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "900000N 1800000E");
}

TEST(FromICAO, S90W180toNOTAM)
{
	std::optional<Coordinates::Values> from{ICAO::from("90S180W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "900000S 1800000W");
}

TEST(FromICAO, ExtraSpaceChars)
{
	std::optional<Coordinates::Values> from{ICAO::from("  505340  N  0105719  E  ")};
	ASSERT_TRUE(from.has_value());
}

TEST(FromICAO, N006000E0000000OutOfRange)
{
	EXPECT_THROW(ICAO::from("006000N0000000E"), MinutesOutOfRange);
}

TEST(FromICAO, S006000E0000000OutOfRange)
{
	EXPECT_THROW(ICAO::from("006000S0000000E"), MinutesOutOfRange);
}

TEST(FromICAO, N000060E0000000OutOfRange)
{
	EXPECT_THROW(ICAO::from("000060N0000000E"), SecondsOutOfRange);
}

TEST(FromICAO, S000060E0000000OutOfRange)
{
	EXPECT_THROW(ICAO::from("000060S0000000E"), SecondsOutOfRange);
}

TEST(FromICAO, N900001E0000000OutOfRange)
{
	EXPECT_THROW(ICAO::from("900001N0000000E"), LatitudeOutOfRange);
}

TEST(FromICAO, S900001E0000000OutOfRange)
{
	EXPECT_THROW(ICAO::from("900001S0000000E"), LatitudeOutOfRange);
}

TEST(FromICAO, N000000E3600000OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N3600000E"), DegreesOutOfRange);
}

TEST(FromICAO, N000000W3600000OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N3600000W"), DegreesOutOfRange);
}

TEST(FromICAO, N000000E0006000OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N0006000E"), MinutesOutOfRange);
}

TEST(FromICAO, N000000W0006000OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N0006000W"), MinutesOutOfRange);
}

TEST(FromICAO, N000000E0000060OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N0000060E"), SecondsOutOfRange);
}

TEST(FromICAO, N000000W0000060OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N0000060W"), SecondsOutOfRange);
}

TEST(FromICAO, N000000E1800001OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N1800001E"), LongitudeOutOfRange);
}

TEST(FromICAO, N000000W1800001OutOfRange)
{
	EXPECT_THROW(ICAO::from("000000N1800001W"), LongitudeOutOfRange);
}
