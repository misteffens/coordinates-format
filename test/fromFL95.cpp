//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include <DMS.h>
#include <Decimal.h>
#include <FL95.h>
#include <FPL.h>
#include <NOTAM.h>
#include <gtest/gtest.h>
#include <limits>

#include <DegreesOutOfRange.h>
#include <LatitudeOutOfRange.h>
#include <LongitudeOutOfRange.h>
#include <MinutesOutOfRange.h>

static const std::string tooLarge(std::size_t(std::numeric_limits<double>::max_exponent10) + 1, '9');

// Test name coordinate format: [NS]ddmmm[EW]dddmmm with minutes in deciminutes.

TEST(FromFL95, N00000E000000toDecimal)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 00.0 E 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromFL95, S00000W000000toDecimal)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 00.0 W 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromFL95, N00360E000360toDecimal)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 36.0 E 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.600000, 0.600000");
}

TEST(FromFL95, S00360W000360toDecimal)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 36.0 W 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-0.600000, -0.600000");
}

TEST(FromFL95, N89599E179599toDecimal)
{
	std::optional<Coordinates::Values> from{FL95::from("N 89 59.9 E 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "89.998333, 179.998333");
}

TEST(FromFL95, S89599W179599toDecimal)
{
	std::optional<Coordinates::Values> from{FL95::from("S 89 59.9 W 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-89.998333, -179.998333");
}

TEST(FromFL95, N00000E000000toDMS)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 00.0 E 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromFL95, S00000W000000toDMS)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 00.0 W 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromFL95, N00360E000360toDMS)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 36.0 E 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''N 0°36'0.00''E");
}

TEST(FromFL95, S00360W000360toDMS)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 36.0 W 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''S 0°36'0.00''W");
}

TEST(FromFL95, N89599E179599toDMS)
{
	std::optional<Coordinates::Values> from{FL95::from("N 89 59.9 E 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'54.00''N 179°59'54.00''E");
}

TEST(FromFL95, S89599W179599toDMS)
{
	std::optional<Coordinates::Values> from{FL95::from("S 89 59.9 W 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'54.00''S 179°59'54.00''W");
}

TEST(FromFL95, N00000E000000toFL95)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 00.0 E 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromFL95, S00000W000000toFL95)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 00.0 W 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromFL95, N00360E000360toFL95)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 36.0 E 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 36.0 E 000 36.0");
}

TEST(FromFL95, S00360W000360toFL95)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 36.0 W 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 00 36.0 W 000 36.0");
}

TEST(FromFL95, N89599E179599toFL95)
{
	std::optional<Coordinates::Values> from{FL95::from("N 89 59.9 E 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 89 59.9 E 179 59.9");
}

TEST(FromFL95, S89599W179599toFL95)
{
	std::optional<Coordinates::Values> from{FL95::from("S 89 59.9 W 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 89 59.9 W 179 59.9");
}

TEST(FromFL95, N00000E000000toFPL)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 00.0 E 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromFL95, S00000W000000toFPL)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 00.0 W 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromFL95, N00360E000360toFPL)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 36.0 E 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036N00036E");
}

TEST(FromFL95, S00360W000360toFPL)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 36.0 W 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036S00036W");
}

TEST(FromFL95, N89599E179599toFPL)
{
	std::optional<Coordinates::Values> from{FL95::from("N 89 59.9 E 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000N18000E");
}

TEST(FromFL95, S89599W179599toFPL)
{
	std::optional<Coordinates::Values> from{FL95::from("S 89 59.9 W 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000S18000W");
}

TEST(FromFL95, N00000E000000toNOTAM)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 00.0 E 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromFL95, S00000W000000toNOTAM)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 00.0 W 000 00.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromFL95, N00360E000360toNOTAM)
{
	std::optional<Coordinates::Values> from{FL95::from("N 00 36.0 E 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600N 0003600E");
}

TEST(FromFL95, S00360W000360toNOTAM)
{
	std::optional<Coordinates::Values> from{FL95::from("S 00 36.0 W 000 36.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600S 0003600W");
}

TEST(FromFL95, N89599E179599toNOTAM)
{
	std::optional<Coordinates::Values> from{FL95::from("N 89 59.9 E 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895954N 1795954E");
}

TEST(FromFL95, S89599W179599toNOTAM)
{
	std::optional<Coordinates::Values> from{FL95::from("S 89 59.9 W 179 59.9")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895954S 1795954W");
}

TEST(FromFL95, ExtraSpaceChars)
{
	std::optional<Coordinates::Values> from{FL95::from("  N  50  53.7  E  010  57.3  ")};
	ASSERT_TRUE(from.has_value());
}

TEST(FromFL95, N360000E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 360 00.0 E 000 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, S360000E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("S 360 00.0 E 000 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, NtooLarge000E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("N " + tooLarge + " 00.0 E 000 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, StooLarge000E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("S " + tooLarge + " 00.0 E 000 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, N00600E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 60.0 E 000 00.0"), MinutesOutOfRange);
}

TEST(FromFL95, S00600E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("S 00 60.0 E 000 00.0"), MinutesOutOfRange);
}

TEST(FromFL95, N00tooLarge0E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 " + tooLarge + ".0 E 000 00.0"), MinutesOutOfRange);
}

TEST(FromFL95, S00tooLarge0E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("S 00 " + tooLarge + ".0 E 000 00.0"), MinutesOutOfRange);
}

TEST(FromFL95, N90001E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 90 00.1 E 000 00.0"), LatitudeOutOfRange);
}

TEST(FromFL95, S90001E000000OutOfRange)
{
	EXPECT_THROW(FL95::from("S 90 00.1 E 000 00.0"), LatitudeOutOfRange);
}

TEST(FromFL95, N00000E360000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 E 360 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, N00000W360000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 W 360 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, N00000EtooLarge000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 E " + tooLarge + " 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, N00000WtooLarge000OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 W " + tooLarge + " 00.0"), DegreesOutOfRange);
}

TEST(FromFL95, N00000E000600OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 60.0"), MinutesOutOfRange);
}

TEST(FromFL95, N00000W000600OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 W 000 60.0"), MinutesOutOfRange);
}

TEST(FromFL95, N00000E000tooLarge0OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 " + tooLarge + ".0"), MinutesOutOfRange);
}

TEST(FromFL95, N00000W000tooLarge0OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 W 000 " + tooLarge + ".0"), MinutesOutOfRange);
}

TEST(FromFL95, N00000E180001OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 E 180 00.1"), LongitudeOutOfRange);
}

TEST(FromFL95, N00000W180001OutOfRange)
{
	EXPECT_THROW(FL95::from("N 00 00.0 W 180 00.1"), LongitudeOutOfRange);
}
