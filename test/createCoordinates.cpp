//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include <Coordinates.h>
#include <gtest/gtest.h>

#include <UnknownInputFormat.h>
#include <UnknownOutputFormat.h>

TEST(Coordinates, FromDecimaltoDecimal)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("decimal", "0.000000, 0.000000")};
	EXPECT_EQ(coordinates->toString(), "0.000000, 0.000000");
}

TEST(Coordinates, FromDecimaltoDMS)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("dms", "0.000000, 0.000000")};
	EXPECT_EQ(coordinates->toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(Coordinates, FromDecimaltoFL95)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fl95", "0.000000, 0.000000")};
	EXPECT_EQ(coordinates->toString(), "N 00 00.0 E 000 00.0");
}

TEST(Coordinates, FromDecimaltoFPL)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fpl", "0.000000, 0.000000")};
	EXPECT_EQ(coordinates->toString(), "0000N00000E");
}

TEST(Coordinates, FromDecimaltoNOTAM)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("notam", "0.000000, 0.000000")};
	EXPECT_EQ(coordinates->toString(), "000000N 0000000E");
}

TEST(Coordinates, FromDecimaltoInvalid)
{
	EXPECT_THROW(Coordinates::create("invalid", "0.000000, 0.000000"), std::range_error);
}

TEST(Coordinates, FromDMStoDecimal)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("decimal", "0°0'0.00''N 0°0'0.00''E")};
	EXPECT_EQ(coordinates->toString(), "0.000000, 0.000000");
}

TEST(Coordinates, FromDMStoDMS)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("dms", "0°0'0.00''N 0°0'0.00''E")};
	EXPECT_EQ(coordinates->toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(Coordinates, FromDMStoFL95)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fl95", "0°0'0.00''N 0°0'0.00''E")};
	EXPECT_EQ(coordinates->toString(), "N 00 00.0 E 000 00.0");
}

TEST(Coordinates, FromDMStoFPL)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fpl", "0°0'0.00''N 0°0'0.00''E")};
	EXPECT_EQ(coordinates->toString(), "0000N00000E");
}

TEST(Coordinates, FromDMStoNOTAM)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("notam", "0°0'0.00''N 0°0'0.00''E")};
	EXPECT_EQ(coordinates->toString(), "000000N 0000000E");
}

TEST(Coordinates, FromDMStoInvalid)
{
	EXPECT_THROW(Coordinates::create("invalid", "0°0'0.00''N 0°0'0.00''E"), UnknownOutputFormat);
}

TEST(Coordinates, FromFL95toDecimal)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("decimal", "N 00 00.0 E 000 00.0")};
	EXPECT_EQ(coordinates->toString(), "0.000000, 0.000000");
}

TEST(Coordinates, FromFL95toDMS)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("dms", "N 00 00.0 E 000 00.0")};
	EXPECT_EQ(coordinates->toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(Coordinates, FromFL95toFL95)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fl95", "N 00 00.0 E 000 00.0")};
	EXPECT_EQ(coordinates->toString(), "N 00 00.0 E 000 00.0");
}

TEST(Coordinates, FromFL95toFPL)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fpl", "N 00 00.0 E 000 00.0")};
	EXPECT_EQ(coordinates->toString(), "0000N00000E");
}

TEST(Coordinates, FromFL95toNOTAM)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("notam", "N 00 00.0 E 000 00.0")};
	EXPECT_EQ(coordinates->toString(), "000000N 0000000E");
}

TEST(Coordinates, FromFL95toInvalid)
{
	EXPECT_THROW(Coordinates::create("invalid", "N 00 00.0 E 000 00.0"), UnknownOutputFormat);
}

TEST(Coordinates, FromFPLtoDecimal)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("decimal", "0000N00000E")};
	EXPECT_EQ(coordinates->toString(), "0.000000, 0.000000");
}

TEST(Coordinates, FromFPLtoDMS)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("dms", "0000N00000E")};
	EXPECT_EQ(coordinates->toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(Coordinates, FromFPLtoFL95)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fl95", "0000N00000E")};
	EXPECT_EQ(coordinates->toString(), "N 00 00.0 E 000 00.0");
}

TEST(Coordinates, FromFPLtoFPL)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fpl", "0000N00000E")};
	EXPECT_EQ(coordinates->toString(), "0000N00000E");
}

TEST(Coordinates, FromFPLtoNOTAM)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("notam", "0000N00000E")};
	EXPECT_EQ(coordinates->toString(), "000000N 0000000E");
}

TEST(Coordinates, FromFPLtoInvalid)
{
	EXPECT_THROW(Coordinates::create("invalid", "0000N00000E"), UnknownOutputFormat);
}

TEST(Coordinates, FromNOTAMtoDecimal)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("decimal", "000000N 0000000E")};
	EXPECT_EQ(coordinates->toString(), "0.000000, 0.000000");
}

TEST(Coordinates, FromNOTAMtoDMS)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("dms", "000000N 0000000E")};
	EXPECT_EQ(coordinates->toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(Coordinates, FromNOTAMtoFL95)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fl95", "000000N 0000000E")};
	EXPECT_EQ(coordinates->toString(), "N 00 00.0 E 000 00.0");
}

TEST(Coordinates, FromNOTAMtoFPL)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("fpl", "000000N 0000000E")};
	EXPECT_EQ(coordinates->toString(), "0000N00000E");
}

TEST(Coordinates, FromNOTAMtoNOTAM)
{
	Coordinates::UniquePtr coordinates{Coordinates::create("notam", "000000N 0000000E")};
	EXPECT_EQ(coordinates->toString(), "000000N 0000000E");
}

TEST(Coordinates, FromNOTAMtoInvalid)
{
	EXPECT_THROW(Coordinates::create("invalid", "000000N 0000000E"), UnknownOutputFormat);
}

TEST(Coordinates, FromInvalidtoDecimal)
{
	EXPECT_THROW(Coordinates::create("decimal", "invalid"), UnknownInputFormat);
}

TEST(Coordinates, FromInvalidtoDMS)
{
	EXPECT_THROW(Coordinates::create("dms", "invalid"), UnknownInputFormat);
}

TEST(Coordinates, FromInvalidtoFL95)
{
	EXPECT_THROW(Coordinates::create("fl95", "invalid"), UnknownInputFormat);
}

TEST(Coordinates, FromInvalidtoFPL)
{
	EXPECT_THROW(Coordinates::create("fpl", "invalid"), UnknownInputFormat);
}

TEST(Coordinates, FromInvalidtoNOTAM)
{
	EXPECT_THROW(Coordinates::create("notam", "invalid"), UnknownInputFormat);
}

TEST(Coordinates, FromInvalidtoInvalid)
{
	EXPECT_THROW(Coordinates::create("invalid", "invalid"), UnknownInputFormat);
}
