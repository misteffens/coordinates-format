//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include <DMS.h>
#include <Decimal.h>
#include <FL95.h>
#include <FPL.h>
#include <NOTAM.h>
#include <gtest/gtest.h>
#include <limits>

#include <DegreesOutOfRange.h>
#include <LatitudeOutOfRange.h>
#include <LongitudeOutOfRange.h>

static const std::string tooLarge(std::size_t(std::numeric_limits<double>::max_exponent10) + 1, '9');

// Test name coordinate format: [NS]ddmmssss[EW]dddmmssss with seconds in centiseconds.

TEST(FromDecimal, N00000000E000000000toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.0, 0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromDecimal, S00000000W000000000toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.0, -0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromDecimal, N00360000E000360000toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.6, 0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.600000, 0.600000");
}

TEST(FromDecimal, S00360000W000360000toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.6, -0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-0.600000, -0.600000");
}

TEST(FromDecimal, N89595900E179595900toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99972222222222, 179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "89.999722, 179.999722");
}

TEST(FromDecimal, S89595900W179595900toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99972222222222, -179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-89.999722, -179.999722");
}

TEST(FromDecimal, N89595999E179595999toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99999722222222, 179.99999722222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "89.999997, 179.999997");
}

TEST(FromDecimal, S89595999W179595999toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99999722222222, -179.99999722222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-89.999997, -179.999997");
}

TEST(FromDecimal, N01140240W005404080toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("+1.234 -5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "1.234000, -5.678000");
}

TEST(FromDecimal, S01140240E005404080toDecimal)
{
	std::optional<Coordinates::Values> from{Decimal::from("-1.234 +5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-1.234000, 5.678000");
}

TEST(FromDecimal, N00000000E000000000toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.0, 0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromDecimal, S00000000W000000000toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.0, -0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromDecimal, N00360000E000360000toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.6, 0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''N 0°36'0.00''E");
}

TEST(FromDecimal, S00360000W000360000toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.6, -0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''S 0°36'0.00''W");
}

TEST(FromDecimal, N89595900E179595900toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99972222222222, 179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.00''N 179°59'59.00''E");
}

TEST(FromDecimal, S89595900W179595900toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99972222222222, -179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.00''S 179°59'59.00''W");
}

TEST(FromDecimal, N89595999E179595999toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99999722222222, 179.99999722222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.99''N 179°59'59.99''E");
}

TEST(FromDecimal, S89595999W179595999toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99999722222222, -179.99999722222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.99''S 179°59'59.99''W");
}

TEST(FromDecimal, N01140240W005404080toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("+1.234 -5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "1°14'2.40''N 5°40'40.80''W");
}

TEST(FromDecimal, S01140240E005404080toDMS)
{
	std::optional<Coordinates::Values> from{Decimal::from("-1.234 +5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "1°14'2.40''S 5°40'40.80''E");
}

TEST(FromDecimal, N00000000E000000000toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.0, 0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromDecimal, S00000000W000000000toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.0, -0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromDecimal, N00360000E000360000toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.6, 0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 36.0 E 000 36.0");
}

TEST(FromDecimal, S00360000W000360000toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.6, -0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 00 36.0 W 000 36.0");
}

TEST(FromDecimal, N89595400E179595400toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99833333333333, 179.99833333333333")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 89 59.9 E 179 59.9");
}

TEST(FromDecimal, S89595400W179595400toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99833333333333, -179.99833333333333")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 89 59.9 W 179 59.9");
}

TEST(FromDecimal, N89595900E179595900toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99972222222222, 179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 90 00.0 E 180 00.0");
}

TEST(FromDecimal, S89595900W179595900toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99972222222222, -179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 90 00.0 W 180 00.0");
}

TEST(FromDecimal, N01140240W005404080toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("+1.234 -5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 01 14.0 W 005 40.7");
}

TEST(FromDecimal, S01140240E005404080toFL95)
{
	std::optional<Coordinates::Values> from{Decimal::from("-1.234 +5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 01 14.0 E 005 40.7");
}

TEST(FromDecimal, N00000000E000000000toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.0, 0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromDecimal, S00000000W000000000toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.0, -0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromDecimal, N00360000E000360000toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.6, 0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036N00036E");
}

TEST(FromDecimal, S00360000W000360000toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.6, -0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036S00036W");
}

TEST(FromDecimal, N89595900E179595900toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99972222222222, 179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000N18000E");
}

TEST(FromDecimal, S89595900W179595900toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99972222222222, -179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000S18000W");
}

TEST(FromDecimal, N01140240W005404080toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("+1.234 -5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0114N00541W");
}

TEST(FromDecimal, S01140240E005404080toFPL)
{
	std::optional<Coordinates::Values> from{Decimal::from("-1.234 +5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0114S00541E");
}

TEST(FromDecimal, N00000000E000000000toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.0, 0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromDecimal, S00000000W000000000toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.0, -0.0")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromDecimal, N00360000E000360000toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("0.6, 0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600N 0003600E");
}

TEST(FromDecimal, S00360000W000360000toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("-0.6, -0.6")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600S 0003600W");
}

TEST(FromDecimal, N89595900E179595900toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("89.99972222222222, 179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895959N 1795959E");
}

TEST(FromDecimal, S89595900W179595900toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("-89.99972222222222, -179.99972222222222")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895959S 1795959W");
}

TEST(FromDecimal, N01140240W005404080toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("+1.234 -5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "011402N 0054041W");
}

TEST(FromDecimal, S01140240E005404080toNOTAM)
{
	std::optional<Coordinates::Values> from{Decimal::from("-1.234 +5.678")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "011402S 0054041E");
}

TEST(FromDecimal, ExtraSpaceChars)
{
	std::optional<Coordinates::Values> from{Decimal::from("  50.89436123755187  ,   10.955342136148163  ")};
	ASSERT_TRUE(from.has_value());
}

TEST(FromDecimal, N360000000E000000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("360.000000, 0.0"), DegreesOutOfRange);
}

TEST(FromDecimal, S360000000E000000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("-360.000000, 0.0"), DegreesOutOfRange);
}

TEST(FromDecimal, NtooLarge000000E000000000OutOfRange)
{
	EXPECT_THROW(Decimal::from(tooLarge + ".000000, 0.0"), DegreesOutOfRange);
}

TEST(FromDecimal, StooLarge000000E000000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("-" + tooLarge + ".000000, 0.0"), DegreesOutOfRange);
}

TEST(FromDecimal, N90000001E000000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("90.000003, 0.0"), LatitudeOutOfRange);
}

TEST(FromDecimal, S90000001E000000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("-90.000003, 0.0"), LatitudeOutOfRange);
}

TEST(FromDecimal, N00000000E360000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("0.0, 360.000000"), DegreesOutOfRange);
}

TEST(FromDecimal, N00000000W360000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("0.0, -360.000000"), DegreesOutOfRange);
}

TEST(FromDecimal, N00000000EtooLarge000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("0.0, " + tooLarge + ".000000"), DegreesOutOfRange);
}

TEST(FromDecimal, N00000000WtooLarge000000OutOfRange)
{
	EXPECT_THROW(Decimal::from("0.0, -" + tooLarge + ".000000"), DegreesOutOfRange);
}

TEST(FromDecimal, N00000000E180000001OutOfRange)
{
	EXPECT_THROW(Decimal::from("0.0, 180.000003"), LongitudeOutOfRange);
}

TEST(FromDecimal, N00000000W180000001OutOfRange)
{
	EXPECT_THROW(Decimal::from("0.0, -180.000003"), LongitudeOutOfRange);
}
