//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef CharconvMock_h_INCLUDED
#define CharconvMock_h_INCLUDED
#include <charconv>
#include <gmock/gmock.h>

class CharconvMock
{
public:
	CharconvMock()
	{
		ON_CALL(*this, from_chars(testing::_, testing::_, testing::An<unsigned long&>(), testing::_))
			.WillByDefault([this](char const* first, char const* last, unsigned long& value, int base) {
				return fromChars(first, last, value, base);
			});
		ON_CALL(*this, from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
			.WillByDefault([this](char const* first, char const* last, double& value, std::chars_format fmt) {
				return fromChars(first, last, value, fmt);
			});
	}

	static CharconvMock& getInstance();
	MOCK_METHOD(std::from_chars_result, from_chars, (char const* first, char const* last, unsigned long& value, int base));
	MOCK_METHOD(std::from_chars_result, from_chars, (char const* first, char const* last, double& value, std::chars_format fmt));

private:
	static std::from_chars_result fromChars(char const* first, char const* last, unsigned long& value, int base);
	static std::from_chars_result fromChars(char const* first, char const* last, double& value, std::chars_format fmt);
};

#endif // CharconvMock_h_INCLUDED
