//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include <DMS.h>
#include <Decimal.h>
#include <FL95.h>
#include <FPL.h>
#include <NOTAM.h>
#include <gtest/gtest.h>
#include <limits>

#include <DegreesOutOfRange.h>
#include <LatitudeOutOfRange.h>
#include <LongitudeOutOfRange.h>
#include <MinutesOutOfRange.h>
#include <SecondsOutOfRange.h>

static const std::string tooLarge(std::size_t(std::numeric_limits<double>::max_exponent10) + 1, '9');

// Test name coordinate format: [NS]ddmmssss[EW]dddmmssss with seconds in centiseconds.

TEST(FromDMS, N00000000E000000000toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''N 0°0'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromDMS, S00000000W000000000toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''S 0°0'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.000000, 0.000000");
}

TEST(FromDMS, N00360000E000360000toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''N 0°36'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "0.600000, 0.600000");
}

TEST(FromDMS, S00360000W000360000toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''S 0°36'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-0.600000, -0.600000");
}

TEST(FromDMS, N89595900E179595900toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''N 179°59'59.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "89.999722, 179.999722");
}

TEST(FromDMS, S89595900W179595900toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''S 179°59'59.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-89.999722, -179.999722");
}

TEST(FromDMS, N89595999E179595999toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.99''N 179°59'59.99''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "89.999997, 179.999997");
}

TEST(FromDMS, S89595999W179595999toDecimal)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.99''S 179°59'59.99''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(Decimal{from.value()}.toString(), "-89.999997, -179.999997");
}

TEST(FromDMS, N00000000E000000000toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''N 0°0'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromDMS, S00000000W000000000toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''S 0°0'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°0'0.00''N 0°0'0.00''E");
}

TEST(FromDMS, N00360000E000360000toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''N 0°36'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''N 0°36'0.00''E");
}

TEST(FromDMS, S00360000W000360000toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''S 0°36'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "0°36'0.00''S 0°36'0.00''W");
}

TEST(FromDMS, N89595900E179595900toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''N 179°59'59.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.00''N 179°59'59.00''E");
}

TEST(FromDMS, S89595900W179595900toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''S 179°59'59.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.00''S 179°59'59.00''W");
}

TEST(FromDMS, N89595999E179595999toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.99''N 179°59'59.99''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.99''N 179°59'59.99''E");
}

TEST(FromDMS, S89595999W179595999toDMS)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.99''S 179°59'59.99''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(DMS{from.value()}.toString(), "89°59'59.99''S 179°59'59.99''W");
}

TEST(FromDMS, N00000000E000000000toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''N 0°0'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromDMS, S00000000W000000000toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''S 0°0'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 00.0 E 000 00.0");
}

TEST(FromDMS, N00360000E000360000toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''N 0°36'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 00 36.0 E 000 36.0");
}

TEST(FromDMS, S00360000W000360000toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''S 0°36'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 00 36.0 W 000 36.0");
}

TEST(FromDMS, N89595400E179595400toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'54.00''N 179°59'54.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 89 59.9 E 179 59.9");
}

TEST(FromDMS, S89595400W179595400toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'54.00''S 179°59'54.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 89 59.9 W 179 59.9");
}

TEST(FromDMS, N89595900E179595900toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''N 179°59'59.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "N 90 00.0 E 180 00.0");
}

TEST(FromDMS, S89595900W179595900toFL95)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''S 179°59'59.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FL95{from.value()}.toString(), "S 90 00.0 W 180 00.0");
}

TEST(FromDMS, N00000000E000000000toFPL)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''N 0°0'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromDMS, S00000000W000000000toFPL)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''S 0°0'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0000N00000E");
}

TEST(FromDMS, N00360000E000360000toFPL)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''N 0°36'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036N00036E");
}

TEST(FromDMS, S00360000W000360000toFPL)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''S 0°36'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "0036S00036W");
}

TEST(FromDMS, N89595900E179595900toFPL)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''N 179°59'59.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000N18000E");
}

TEST(FromDMS, S89595900W179595900toFPL)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''S 179°59'59.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(FPL{from.value()}.toString(), "9000S18000W");
}

TEST(FromDMS, N00000000E000000000toNOTAM)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''N 0°0'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromDMS, S00000000W000000000toNOTAM)
{
	std::optional<Coordinates::Values> from{DMS::from("0°0'0.00''S 0°0'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "000000N 0000000E");
}

TEST(FromDMS, N00360000E000360000toNOTAM)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''N 0°36'0.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600N 0003600E");
}

TEST(FromDMS, S00360000W000360000toNOTAM)
{
	std::optional<Coordinates::Values> from{DMS::from("0°36'0.00''S 0°36'0.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "003600S 0003600W");
}

TEST(FromDMS, N89595900E179595900toNOTAM)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''N 179°59'59.00''E")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895959N 1795959E");
}

TEST(FromDMS, S89595900W179595900toNOTAM)
{
	std::optional<Coordinates::Values> from{DMS::from("89°59'59.00''S 179°59'59.00''W")};
	ASSERT_TRUE(from.has_value());
	EXPECT_EQ(NOTAM{from.value()}.toString(), "895959S 1795959W");
}

TEST(FromDMS, ExtraSpaceChars)
{
	std::optional<Coordinates::Values> from{DMS::from("  50  °  53  '  39.70  ''  N  10  °  57  '  19.23  ''  E  ")};
	ASSERT_TRUE(from.has_value());
}

TEST(FromDMS, N360000000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("360°0'0.00''N 0°0'0.00''E"), DegreesOutOfRange);
}

TEST(FromDMS, S360000000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("360°0'0.00''S 0°0'0.00''E"), DegreesOutOfRange);
}

TEST(FromDMS, NtooLarge000000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from(tooLarge + "°0'0.00''N 0°0'0.00''E"), DegreesOutOfRange);
}

TEST(FromDMS, StooLarge000000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from(tooLarge + "°0'0.00''S 0°0'0.00''E"), DegreesOutOfRange);
}

TEST(FromDMS, N00600000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°60'0.00''N 0°0'0.00''E"), MinutesOutOfRange);
}

TEST(FromDMS, S00600000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°60'0.00''S 0°0'0.00''E"), MinutesOutOfRange);
}

TEST(FromDMS, N00tooLarge0000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°" + tooLarge + "'0.00''N 0°0'0.00''E"), MinutesOutOfRange);
}

TEST(FromDMS, S00tooLarge0000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°" + tooLarge + "'0.00''S 0°0'0.00''E"), MinutesOutOfRange);
}

TEST(FromDMS, N00006000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'60.00''N 0°0'0.00''E"), SecondsOutOfRange);
}

TEST(FromDMS, S00006000E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'60.00''S 0°0'0.00''E"), SecondsOutOfRange);
}

TEST(FromDMS, N0000tooLarge00E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'" + tooLarge + ".00''N 0°0'0.00''E"), SecondsOutOfRange);
}

TEST(FromDMS, S0000tooLarge00E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'" + tooLarge + ".00''S 0°0'0.00''E"), SecondsOutOfRange);
}

TEST(FromDMS, N90000001E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("90°0'0.01''N 0°0'0.00''E"), LatitudeOutOfRange);
}

TEST(FromDMS, S90000001E000000000OutOfRange)
{
	EXPECT_THROW(DMS::from("90°0'0.01''S 0°0'0.00''E"), LatitudeOutOfRange);
}

TEST(FromDMS, N00000000E360000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 360°0'0.00''E"), DegreesOutOfRange);
}

TEST(FromDMS, N00000000W360000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 360°0'0.00''W"), DegreesOutOfRange);
}

TEST(FromDMS, N00000000EtooLarge000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N " + tooLarge + "°0'0.00''E"), DegreesOutOfRange);
}

TEST(FromDMS, N00000000WtooLarge000000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N " + tooLarge + "°0'0.00''W"), DegreesOutOfRange);
}

TEST(FromDMS, N00000000E000600000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°60'0.00''E"), MinutesOutOfRange);
}

TEST(FromDMS, N00000000W000600000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°60'0.00''W"), MinutesOutOfRange);
}

TEST(FromDMS, N00000000E000tooLarge0000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°" + tooLarge + "'0.00''E"), MinutesOutOfRange);
}

TEST(FromDMS, N00000000W000tooLarge0000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°" + tooLarge + "'0.00''W"), MinutesOutOfRange);
}

TEST(FromDMS, N00000000E000006000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'60.00''E"), SecondsOutOfRange);
}

TEST(FromDMS, N00000000W000006000OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'60.00''W"), SecondsOutOfRange);
}

TEST(FromDMS, N00000000E00000tooLarge00OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'" + tooLarge + ".00''E"), SecondsOutOfRange);
}

TEST(FromDMS, N00000000W00000tooLarge00OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'" + tooLarge + ".00''W"), SecondsOutOfRange);
}

TEST(FromDMS, N00000000E180000001OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 180°0'0.01''E"), LongitudeOutOfRange);
}

TEST(FromDMS, N00000000W180000001OutOfRange)
{
	EXPECT_THROW(DMS::from("0°0'0.00''N 180°0'0.01''W"), LongitudeOutOfRange);
}
