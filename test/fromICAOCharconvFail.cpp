//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "CharconvMock.h"
#include <ICAO.h>
#include <gtest/gtest.h>

TEST(FromICAO, N000000E0000000Success)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(6)
		.InSequence(s);
	EXPECT_NO_THROW(ICAO::from("000000N0000000E"));
}

TEST(FromICAO, N000000E0000000Incomplete1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000Incomplete2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000Incomplete3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000Incomplete4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000Incomplete5)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(4)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000Incomplete6)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(5)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000InvalidArgument1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000InvalidArgument2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000InvalidArgument3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000InvalidArgument4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000InvalidArgument5)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(4)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N000000E0000000InvalidArgument6)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(5)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("000000N0000000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000Success)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(4)
		.InSequence(s);
	EXPECT_NO_THROW(ICAO::from("0000N00000E"));
}

TEST(FromICAO, N0000E00000Incomplete1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000Incomplete2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000Incomplete3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000Incomplete4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000InvalidArgument1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000InvalidArgument2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000InvalidArgument3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}

TEST(FromICAO, N0000E00000InvalidArgument4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(ICAO::from("0000N00000E"), std::logic_error);
}
