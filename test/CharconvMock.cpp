//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "CharconvMock.h"
#include <cstdlib>
#include <string>

CharconvMock& CharconvMock::getInstance()
{
	static testing::StrictMock<CharconvMock> instance{};
	return instance;
}

std::from_chars_result CharconvMock::fromChars(char const* first, char const* last, unsigned long& value, int base)
{
	std::string in;
	in.reserve((last - first) + 1);
	in.assign(first, last);
	in.push_back('\0');
	char* end;
	value = strtoul(in.data(), &end, base);
	return std::from_chars_result{first + (end - in.data()), std::errc{}};
};

std::from_chars_result CharconvMock::fromChars(char const* first, char const* last, double& value, std::chars_format)
{
	std::string in;
	in.reserve((last - first) + 1);
	in.assign(first, last);
	in.push_back('\0');
	char* end;
	value = strtod(in.data(), &end);
	return std::from_chars_result{first + (end - in.data()), std::errc{}};
};

namespace std {

std::from_chars_result from_chars(char const* first, char const* last, unsigned long& value, int base)
{
	return CharconvMock::getInstance().from_chars(first, last, value, base);
}

std::from_chars_result from_chars(char const* first, char const* last, double& value, std::chars_format fmt)
{
	return CharconvMock::getInstance().from_chars(first, last, value, fmt);
}

} // namespace std
