//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include <gtest/gtest.h>
#include <string>

#include <UnknownInputFormat.h>
#include <UnknownOutputFormat.h>
#include <UsageError.h>

int testeeMain(int argc, char* argv[]);

static const std::string usageErrorMessage{std::string{UsageError{}.what()} + "\n"};
static const std::string unknownInputFormatMessage{std::string{UnknownInputFormat{}.what()} + "\n"};
static const std::string unknownOutputFormatMessage{std::string{UnknownOutputFormat{}.what()} + "\n"};

static int testMain(std::vector<char const*> argv)
{
	return testeeMain(argv.size(), const_cast<char**>(argv.data()));
}

TEST(Main, Usage)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({""}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), usageErrorMessage);
}

TEST(Main, FromDecimaltoDecimal)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "decimal", "0.000000, 0.000000"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0.000000, 0.000000\n");
}

TEST(Main, FromDecimaltoDMS)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "dms", "0.000000, 0.000000"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0°0'0.00''N 0°0'0.00''E\n");
}

TEST(Main, FromDecimaltoFL95)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fl95", "0.000000, 0.000000"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "N 00 00.0 E 000 00.0\n");
}

TEST(Main, FromDecimaltoFPL)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fpl", "0.000000, 0.000000"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0000N00000E\n");
}

TEST(Main, FromDecimaltoNOTAM)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "notam", "0.000000, 0.000000"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "000000N 0000000E\n");
}

TEST(Main, FromDecimaltoInvalid)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "invalid", "0.000000, 0.000000"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownOutputFormatMessage);
}

TEST(Main, FromDMStoDecimal)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "decimal", "0°0'0.00''N 0°0'0.00''E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0.000000, 0.000000\n");
}

TEST(Main, FromDMStoDMS)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "dms", "0°0'0.00''N 0°0'0.00''E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0°0'0.00''N 0°0'0.00''E\n");
}

TEST(Main, FromDMStoFL95)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fl95", "0°0'0.00''N 0°0'0.00''E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "N 00 00.0 E 000 00.0\n");
}

TEST(Main, FromDMStoFPL)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fpl", "0°0'0.00''N 0°0'0.00''E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0000N00000E\n");
}

TEST(Main, FromDMStoNOTAM)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "notam", "0°0'0.00''N 0°0'0.00''E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "000000N 0000000E\n");
}

TEST(Main, FromDMStoInvalid)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "invalid", "0°0'0.00''N 0°0'0.00''E"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownOutputFormatMessage);
}

TEST(Main, FromFL95toDecimal)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "decimal", "N 00 00.0 E 000 00.0"}), 0);

	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0.000000, 0.000000\n");
}

TEST(Main, FromFL95toDMS)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "dms", "N 00 00.0 E 000 00.0"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0°0'0.00''N 0°0'0.00''E\n");
}

TEST(Main, FromFL95toFL95)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fl95", "N 00 00.0 E 000 00.0"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "N 00 00.0 E 000 00.0\n");
}

TEST(Main, FromFL95toFPL)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fpl", "N 00 00.0 E 000 00.0"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0000N00000E\n");
}

TEST(Main, FromFL95toNOTAM)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "notam", "N 00 00.0 E 000 00.0"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "000000N 0000000E\n");
}

TEST(Main, FromFL95toInvalid)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "invalid", "N 00 00.0 E 000 00.0"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownOutputFormatMessage);
}

TEST(Main, FromFPLtoDecimal)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "decimal", "0000N00000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0.000000, 0.000000\n");
}

TEST(Main, FromFPLtoDMS)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "dms", "0000N00000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0°0'0.00''N 0°0'0.00''E\n");
}

TEST(Main, FromFPLtoFL95)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fl95", "0000N00000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "N 00 00.0 E 000 00.0\n");
}

TEST(Main, FromFPLtoFPL)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fpl", "0000N00000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0000N00000E\n");
}

TEST(Main, FromFPLtoNOTAM)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "notam", "0000N00000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "000000N 0000000E\n");
}

TEST(Main, FromFPLtoInvalid)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "invalid", "0000N00000E"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownOutputFormatMessage);
}

TEST(Main, FromNOTAMtoDecimal)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "decimal", "000000N 0000000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0.000000, 0.000000\n");
}

TEST(Main, FromNOTAMtoDMS)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "dms", "000000N 0000000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0°0'0.00''N 0°0'0.00''E\n");
}

TEST(Main, FromNOTAMtoFL95)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fl95", "000000N 0000000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "N 00 00.0 E 000 00.0\n");
}

TEST(Main, FromNOTAMtoFPL)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "fpl", "000000N 0000000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "0000N00000E\n");
}

TEST(Main, FromNOTAMtoNOTAM)
{
	testing::internal::CaptureStdout();
	EXPECT_EQ(testMain({"", "notam", "000000N 0000000E"}), 0);
	EXPECT_EQ(testing::internal::GetCapturedStdout(), "000000N 0000000E\n");
}

TEST(Main, FromNOTAMtoInvalid)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "invalid", "000000N0000000E"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownOutputFormatMessage);
}

TEST(Main, FromInvalidtoDecimal)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "decimal", "invalid"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownInputFormatMessage);
}

TEST(Main, FromInvalidtoDMS)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "dms", "invalid"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownInputFormatMessage);
}

TEST(Main, FromInvalidtoFL95)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "fl95", "invalid"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownInputFormatMessage);
}

TEST(Main, FromInvalidtoFPL)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "fpl", "invalid"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownInputFormatMessage);
}

TEST(Main, FromInvalidtoNOTAM)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "notam", "invalid"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownInputFormatMessage);
}

TEST(Main, FromInvalidtoInvalid)
{
	testing::internal::CaptureStderr();
	EXPECT_EQ(testMain({"", "invalid", "invalid"}), 1);
	EXPECT_EQ(testing::internal::GetCapturedStderr(), unknownInputFormatMessage);
}
