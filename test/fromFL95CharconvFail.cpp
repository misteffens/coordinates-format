//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "CharconvMock.h"
#include <FL95.h>
#include <gtest/gtest.h>

TEST(FromFL95, Success)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(4)
		.InSequence(s);
	EXPECT_NO_THROW(FL95::from("N 00 00.0 E 000 00.0"));
}

TEST(FromFL95, Incomplete1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}

TEST(FromFL95, Incomplete2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}

TEST(FromFL95, Incomplete3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}

TEST(FromFL95, Incomplete4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}

TEST(FromFL95, InvalidArgument1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}

TEST(FromFL95, InvalidArgument2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}

TEST(FromFL95, InvalidArgument3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}

TEST(FromFL95, InvalidArgument4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(FL95::from("N 00 00.0 E 000 00.0"), std::logic_error);
}
