//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "MockMemoryResource.h"
#include <FL95.h>
#include <gtest/gtest.h>

class FromFL95 : public testing::Test
{
public:
	FromFL95() : defaultResource{std::pmr::get_default_resource()}, mockResource{defaultResource}
	{
	}

protected:
	void SetUp() override
	{
		std::pmr::set_default_resource(&mockResource);
	}

	void TearDown() override
	{
		std::pmr::set_default_resource(defaultResource);
	}

	std::pmr::memory_resource* defaultResource;
	MockMemoryResource mockResource;
};

TEST_F(FromFL95, Success)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(3).InSequence(s);
	EXPECT_NO_THROW(FL95{FL95::from("N 00 00.0 E 000 00.0").value()}.toString());
}

TEST_F(FromFL95, FailAllocate1)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(FL95{FL95::from("N 00 00.0 E 000 00.0").value()}.toString(), std::bad_alloc);
}

TEST_F(FromFL95, FailAllocate2)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(FL95{FL95::from("N 00 00.0 E 000 00.0").value()}.toString(), std::bad_alloc);
}

TEST_F(FromFL95, FailAllocate3)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(FL95{FL95::from("N 00 00.0 E 000 00.0").value()}.toString(), std::bad_alloc);
}
