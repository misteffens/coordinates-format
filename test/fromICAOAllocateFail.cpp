//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "MockMemoryResource.h"
#include <FPL.h>
#include <ICAO.h>
#include <NOTAM.h>
#include <gtest/gtest.h>

class FromICAO : public testing::Test
{
public:
	FromICAO() : defaultResource{std::pmr::get_default_resource()}, mockResource{defaultResource}
	{
	}

protected:
	void SetUp() override
	{
		std::pmr::set_default_resource(&mockResource);
	}

	void TearDown() override
	{
		std::pmr::set_default_resource(defaultResource);
	}

	std::pmr::memory_resource* defaultResource;
	MockMemoryResource mockResource;
};

TEST_F(FromICAO, ToFPLSuccess)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
	EXPECT_NO_THROW(FPL{ICAO::from("000000N0000000E").value()}.toString());
}

TEST_F(FromICAO, ToFPLFailAllocate1)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(FPL{ICAO::from("000000N0000000E").value()}.toString(), std::bad_alloc);
}

TEST_F(FromICAO, ToFPLFailAllocate2)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(FPL{ICAO::from("000000N0000000E").value()}.toString(), std::bad_alloc);
}

TEST_F(FromICAO, ToNOTAMSuccess)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(3).InSequence(s);
	EXPECT_NO_THROW(NOTAM{ICAO::from("000000N0000000E").value()}.toString());
}

TEST_F(FromICAO, ToNOTAMFailAllocate1)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(NOTAM{ICAO::from("000000N0000000E").value()}.toString(), std::bad_alloc);
}

TEST_F(FromICAO, ToNOTAMFailAllocate2)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(NOTAM{ICAO::from("000000N0000000E").value()}.toString(), std::bad_alloc);
}

TEST_F(FromICAO, ToNOTAMFailAllocate3)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(NOTAM{ICAO::from("000000N0000000E").value()}.toString(), std::bad_alloc);
}
