//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "CharconvMock.h"
#include <DMS.h>
#include <gtest/gtest.h>

TEST(FromDMS, Success)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(6)
		.InSequence(s);
	EXPECT_NO_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"));
}

TEST(FromDMS, Incomplete1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, Incomplete2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, Incomplete3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, Incomplete4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, Incomplete5)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(4)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, Incomplete6)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(5)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, InvalidArgument1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, InvalidArgument2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, InvalidArgument3)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, InvalidArgument4)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(3)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, InvalidArgument5)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(4)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}

TEST(FromDMS, InvalidArgument6)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(5)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(DMS::from("0°0'0.00''N 0°0'0.00''E"), std::logic_error);
}
