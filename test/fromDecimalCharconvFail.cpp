//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "CharconvMock.h"
#include <Decimal.h>
#include <gtest/gtest.h>

TEST(FromDecimal, Success)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(2)
		.InSequence(s);
	EXPECT_NO_THROW(Decimal::from("0.0, 0.0"));
}

TEST(FromDecimal, Incomplete1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(Decimal::from("0.0, 0.0"), std::logic_error);
}

TEST(FromDecimal, Incomplete2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{first, std::errc{}};
		});
	EXPECT_THROW(Decimal::from("0.0, 0.0"), std::logic_error);
}

TEST(FromDecimal, InvalidArgument1)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(Decimal::from("0.0, 0.0"), std::logic_error);
}

TEST(FromDecimal, InvalidArgument2)
{
	testing::Sequence s;
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.Times(1)
		.InSequence(s);
	EXPECT_CALL(CharconvMock::getInstance(), from_chars(testing::_, testing::_, testing::An<double&>(), testing::_))
		.InSequence(s)
		.WillOnce([this](char const* first, char const* last, double& value, std::chars_format fmt) {
			return std::from_chars_result{last, std::errc::invalid_argument};
		});
	EXPECT_THROW(Decimal::from("0.0, 0.0"), std::logic_error);
}
