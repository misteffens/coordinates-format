//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "MockMemoryResource.h"
#include <Decimal.h>
#include <gtest/gtest.h>

class FromDecimal : public testing::Test
{
public:
	FromDecimal() : defaultResource{std::pmr::get_default_resource()}, mockResource{defaultResource}
	{
	}

protected:
	void SetUp() override
	{
		std::pmr::set_default_resource(&mockResource);
	}

	void TearDown() override
	{
		std::pmr::set_default_resource(defaultResource);
	}

	std::pmr::memory_resource* defaultResource;
	MockMemoryResource mockResource;
};

TEST_F(FromDecimal, Success)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(3).InSequence(s);
	EXPECT_NO_THROW(Decimal{Decimal::from("0.0, 0.0").value()}.toString());
}

TEST_F(FromDecimal, FailAllocate1)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(Decimal{Decimal::from("0.0, 0.0").value()}.toString(), std::bad_alloc);
}

TEST_F(FromDecimal, FailAllocate2)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(1).InSequence(s);
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(Decimal{Decimal::from("0.0, 0.0").value()}.toString(), std::bad_alloc);
}

TEST_F(FromDecimal, FailAllocate3)
{
	testing::Sequence s;
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).Times(2).InSequence(s);
	EXPECT_CALL(mockResource, do_allocate(testing::_, testing::_)).InSequence(s).WillOnce(testing::Throw(std::bad_alloc{}));
	EXPECT_THROW(Decimal{Decimal::from("0.0, 0.0").value()}.toString(), std::bad_alloc);
}
