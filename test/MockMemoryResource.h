//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef MockMemoryResource_h_INCLUDED
#define MockMemoryResource_h_INCLUDED

#include <gmock/gmock.h>
#include <memory_resource>

class MockMemoryResource : public std::pmr::memory_resource
{
public:
	MockMemoryResource(std::pmr::memory_resource* upstream = std::pmr::get_default_resource()) : upstream{upstream}
	{
		ON_CALL(*this, do_allocate).WillByDefault([upstream](std::size_t bytes, std::size_t alignment) {
			return upstream->allocate(bytes, alignment);
		});
	}

	MOCK_METHOD(void*, do_allocate, (std::size_t bytes, std::size_t alignment), (override));

	void do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override
	{
		upstream->deallocate(p, bytes, alignment);
	}

	bool do_is_equal(std::pmr::memory_resource const& other) const noexcept override
	{
		return this == &other;
	}

private:
	std::pmr::memory_resource* upstream;
};

#endif // MockMemoryResource_h_INCLUDED
