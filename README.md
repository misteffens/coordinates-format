# coordinates-format

## Name

Geographic Coordinates Format Conversions (C++20, using https://github.com/fmtlib/fmt if std::format is not found)

## Description

Command line utility to convert latitude and longitude representations.

| Format identifier | Example                       | Used in                   |
|-------------------|-------------------------------|---------------------------|
| decimal           | 50.894361, 10.955342          | https://maps.google.com   |
| dms               | 50°53'39.70''N 10°57'19.23''E | Degrees, minutes, seconds |
| fl95              | N 50 53.7 E 010 57.3          | https://fl95.de           |
| fpl               | 5054N01057E                   | AIS flight plans          |
| notam             | 505340N 0105719E              | AIS NOTAMs                |

## Usage

coordinates-format *target-format-id* *input*

The input coordinate format is recognized automatically with relaxed format rules. All format
latitude magnitudes must not exceed 90°, while longitude magnitudes are limited to 180°.
All minutes and seconds values must be less than 60.

Leading and trailing space characters are trimmed.

### Decimal input

The separating comma is optional, but at least one comma or a space must separate latitude and
longitude. Additional space characters are accepted. Both latitude and longitude can be
prefixed by a '+' or '-' sign character.

### DMS input

Degrees, minutes, and seconds can be specified with any number of digits. Additional space
characters are accepted between latitude and longitude, between numeric values and the '°',
"'", "''", and the direction tokens.

### FL95 input

Degrees and minutes can be specified with any number of digits. Additional space characters
are accepted between latitude and longitude, between numeric values and the direction tokens.

### ICAO (FPL or NOTAM) input

Latitude and longitude must be provided as tokens of consecutive alphanumerical characters,
optionally separated by space characters. The digit pairs for minutes and seconds are
optional.

## Examples

```
$ coordinates-format dms "50.89436123755187, 10.955342136148163"
50°53'39.70''N 10°57'19.23''E
```

```
$ coordinates-format fl95 "50°53'39.70''N 10°57'19.23''E"
N 50 53.7 E 010 57.3
```

```
$ coordinates-format notam "N 50 53.7 E 010 57.3"
505342N 0105718E
```

```
$ coordinates-format fpl "505342N 0105718E"
5054N01057E
```

## License
GPLv3 or later.
