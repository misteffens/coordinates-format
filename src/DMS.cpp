//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "DMS.h"
#include "DegreesOutOfRange.h"
#include "LatitudeOutOfRange.h"
#include "LongitudeOutOfRange.h"
#include "MinutesOutOfRange.h"
#include "SecondsOutOfRange.h"
#include "format.h"
#include "fromSubmatch.h"
#include <cmath>
#include <regex>

DMS::DMS(Coordinates::Values const& other) : Coordinates{other}
{
}

std::optional<Coordinates::Values> DMS::from(std::string_view const& in)
{
	struct Composition
	{
		Composition(
			std::string_view const& in,
			std::pmr::match_results<std::string_view::const_iterator> const& m,
			std::size_t nDegrees,
			std::size_t nMinutes,
			std::size_t nSeconds,
			std::size_t nDirection) :
			degrees{fromSubmatch(in, m, nDegrees, 360.0, [] { throw DegreesOutOfRange{}; })},
			minutes{fromSubmatch(in, m, nMinutes, 60.0, [] { throw MinutesOutOfRange{}; })},
			seconds{fromSubmatch(in, m, nSeconds, 60.0, [] { throw SecondsOutOfRange{}; })},
			direction{in[m.position(nDirection)]}
		{
		}
		double toDouble() const
		{
			double d{degrees + (minutes + seconds / 60.0) / 60.0};
			return direction == 'N' || direction == 'E' ? d : -d;
		}
		char direction;
		double degrees;
		double minutes;
		double seconds;
	};
	static const std::regex re{
		"[[:space:]]*([[:digit:]]+)[[:space:]]*°[[:space:]]*([[:digit:]]+)[[:space:]]*'[[:space:]]*([[:digit:]]+(\\.[[:digit:]]*)?)"
		"[[:space:]]*''[[:space:]]*([NS])"
		"[[:space:]]*([[:digit:]]+)[[:space:]]*°[[:space:]]*([[:digit:]]+)[[:space:]]*'[[:space:]]*([[:digit:]]+(\\.[[:digit:]]*)?)"
		"[[:space:]]*''[[:space:]]*([EW])[[:space:]]*"};
	std::optional<Coordinates::Values> result;
	std::pmr::match_results<std::string_view::const_iterator> m;
	if (std::regex_match(in.begin(), in.end(), m, re)) {
		result = Coordinates::Values{Composition{in, m, 1, 2, 3, 5}.toDouble(), Composition{in, m, 6, 7, 8, 10}.toDouble()};
		if (result.value().latitude < -90.0 || result.value().latitude > 90.0) {
			throw LatitudeOutOfRange{};
		}
		if (result.value().longitude < -180.0 || result.value().longitude > 180.0) {
			throw LongitudeOutOfRange{};
		}
	}
	return result;
}

std::pmr::string DMS::toString() const
{
	struct Decomposition
	{
		Decomposition(char direction, double coordinate) : direction{direction}
		{
			degrees = static_cast<unsigned long>(std::round(std::abs(coordinate) * (60UL * 60UL * 100UL)));
			secondFraction = degrees % 100UL;
			degrees /= 100UL;
			seconds = degrees % 60UL;
			degrees /= 60UL;
			minutes = degrees % 60UL;
			degrees /= 60UL;
		}
		char direction;
		unsigned long degrees{0};
		unsigned long minutes{0};
		unsigned long seconds{0};
		unsigned long secondFraction{0};
	};
	const Decomposition lat{latitude >= 0.0 ? 'N' : 'S', latitude};
	const Decomposition lon{longitude >= 0.0 ? 'E' : 'W', longitude};
	return format(
		"{}°{}'{}.{:02}''{} {}°{}'{}.{:02}''{}",
		lat.degrees,
		lat.minutes,
		lat.seconds,
		lat.secondFraction,
		lat.direction,
		lon.degrees,
		lon.minutes,
		lon.seconds,
		lon.secondFraction,
		lon.direction);
}
