//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "LatitudeOutOfRange.h"

LatitudeOutOfRange::LatitudeOutOfRange() : std::range_error{"Latitude out of range."}
{
}
