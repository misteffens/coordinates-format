//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "Decimal.h"
#include "DegreesOutOfRange.h"
#include "LatitudeOutOfRange.h"
#include "LongitudeOutOfRange.h"
#include "format.h"
#include "fromSubmatch.h"
#include <regex>

Decimal::Decimal(Coordinates::Values const& other) : Coordinates{other}
{
}

std::optional<Coordinates::Values> Decimal::from(std::string_view const& in)
{
	struct Composition
	{
		Composition(
			std::string_view const& in,
			std::pmr::match_results<std::string_view::const_iterator> const& m,
			std::size_t nSign,
			std::size_t nDegrees) :
			sign{m.length(nSign) > 0 ? in[m.position(nSign)] : '\0'}, degrees{fromSubmatch(in, m, nDegrees, 360.0, [] {
				throw DegreesOutOfRange{};
			})}
		{
		}
		double toDouble() const
		{
			return (sign == '-' ? -degrees : degrees);
		}
		char sign;
		double degrees;
	};
	static const std::regex re{
		"[[:space:]]*([+-])?([[:digit:]]+(\\.[[:digit:]]*)?)[[:space:]]*[,[:space:]]"
		"[[:space:]]*([+-])?([[:digit:]]+(\\.[[:digit:]]*)?)[[:space:]]*"};
	std::optional<Coordinates::Values> result;
	std::pmr::match_results<std::string_view::const_iterator> m;
	if (std::regex_match(in.begin(), in.end(), m, re)) {
		result = Coordinates::Values{Composition{in, m, 1, 2}.toDouble(), Composition{in, m, 4, 5}.toDouble()};
		if (result.value().latitude < -90.0 || result.value().latitude > 90.0) {
			throw LatitudeOutOfRange{};
		}
		if (result.value().longitude < -180.0 || result.value().longitude > 180.0) {
			throw LongitudeOutOfRange{};
		}
	}
	return result;
}

std::pmr::string Decimal::toString() const
{
	return format("{:6f}, {:6f}", (latitude == -0.0 ? 0.0 : latitude), (longitude == -0.0 ? 0.0 : longitude));
}
