//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "UnknownOutputFormat.h"

UnknownOutputFormat::UnknownOutputFormat() : std::range_error{"Unknown output format."}
{
}
