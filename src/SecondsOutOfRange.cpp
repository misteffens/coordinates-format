//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "SecondsOutOfRange.h"

SecondsOutOfRange::SecondsOutOfRange() : std::range_error{"Seconds out of range."}
{
}
