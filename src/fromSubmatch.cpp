//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "fromSubmatch.h"
#include "assertOrThrow.h"
#include <charconv>
#include <memory_resource>

template<typename T>
T fromSubmatch(
	std::string_view const& in,
	std::pmr::match_results<std::string_view::const_iterator> const& m,
	std::size_t position,
	T maxValue,
	std::function<void()> outOfRangeF)
{
	T result{};
	if (m.length(position) > 0) {
		const std::string_view matchStr{in.substr(m.position(position), std::size_t(m.length(position)))};
		const std::from_chars_result parseResult{std::from_chars(matchStr.data(), matchStr.data() + matchStr.size(), result)};
		assertOrThrow(
			(parseResult.ec == std::errc{} || parseResult.ec == std::errc::result_out_of_range) &&
			parseResult.ptr == matchStr.data() + matchStr.size());
		if (parseResult.ec == std::errc::result_out_of_range || result >= maxValue) {
			outOfRangeF();
		}
	}
	return result;
}

template double fromSubmatch<double>(
	std::string_view const&,
	std::pmr::match_results<std::string_view::const_iterator> const&,
	std::size_t,
	double,
	std::function<void()>);
