//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "assertOrThrow.h"
#include <stdexcept>

void assertOrThrow(bool condition)
{
	if (!condition) {
		throw std::logic_error{"Internal error."};
	}
}
