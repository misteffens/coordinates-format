//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef assertOrThrow_h_INCLUDED
#define assertOrThrow_h_INCLUDED

void assertOrThrow(bool condition);

#endif // assertOrThrow_h_INCLUDED
