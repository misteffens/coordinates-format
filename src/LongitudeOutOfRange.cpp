//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "LongitudeOutOfRange.h"

LongitudeOutOfRange::LongitudeOutOfRange() : std::range_error{"Longitude out of range."}
{
}
