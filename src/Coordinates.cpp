//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "Coordinates.h"
#include "DMS.h"
#include "Decimal.h"
#include "FL95.h"
#include "FPL.h"
#include "ICAO.h"
#include "NOTAM.h"
#include "UnknownInputFormat.h"
#include "UnknownOutputFormat.h"
#include "allocateUnique.h"

Coordinates::Coordinates(Values const& other) : latitude{other.latitude}, longitude{other.longitude}
{
}

Coordinates::UniquePtr Coordinates::create(std::string_view const& formatId, std::string_view const& in)
{
	std::optional<Values> from;
	if (!(from = Decimal::from(in)).has_value() && !(from = DMS::from(in)).has_value() && !(from = ICAO::from(in)).has_value() &&
		!(from = FL95::from(in)).has_value()) {
		throw UnknownInputFormat{};
	}
	UniquePtr result;
	if (formatId.compare("decimal") == 0) {
		result = allocateUnique<Decimal>(std::pmr::polymorphic_allocator<Decimal>{}, from.value());
	} else if (formatId.compare("dms") == 0) {
		result = allocateUnique<DMS>(std::pmr::polymorphic_allocator<DMS>{}, from.value());
	} else if (formatId.compare("fpl") == 0) {
		result = allocateUnique<FPL>(std::pmr::polymorphic_allocator<FPL>{}, from.value());
	} else if (formatId.compare("notam") == 0) {
		result = allocateUnique<NOTAM>(std::pmr::polymorphic_allocator<NOTAM>{}, from.value());
	} else if (formatId.compare("fl95") == 0) {
		result = allocateUnique<FL95>(std::pmr::polymorphic_allocator<FL95>{}, from.value());
	} else {
		throw UnknownOutputFormat{};
	}
	return result;
}
