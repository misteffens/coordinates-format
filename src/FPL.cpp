//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "FPL.h"
#include "format.h"
#include <cmath>

FPL::FPL(Coordinates::Values const& other) : ICAO{other}
{
}

std::pmr::string FPL::toString() const
{
	struct Decomposition
	{
		Decomposition(char direction, double coordinate) : direction{direction}
		{
			degrees = static_cast<unsigned long>(std::round(std::abs(coordinate) * 60UL));
			minutes = degrees % 60UL;
			degrees /= 60UL;
		}
		char direction;
		unsigned long degrees{0};
		unsigned long minutes{0};
	};
	const Decomposition lat{latitude >= 0.0 ? 'N' : 'S', latitude};
	const Decomposition lon{longitude >= 0.0 ? 'E' : 'W', longitude};
	return format("{:02}{:02}{}{:03}{:02}{}", lat.degrees, lat.minutes, lat.direction, lon.degrees, lon.minutes, lon.direction);
}
