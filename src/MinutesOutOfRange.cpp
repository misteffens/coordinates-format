//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "MinutesOutOfRange.h"

MinutesOutOfRange::MinutesOutOfRange() : std::range_error{"Minutes out of range."}
{
}
