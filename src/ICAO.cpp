//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "ICAO.h"
#include "DegreesOutOfRange.h"
#include "LatitudeOutOfRange.h"
#include "LongitudeOutOfRange.h"
#include "MinutesOutOfRange.h"
#include "SecondsOutOfRange.h"
#include "fromSubmatch.h"
#include <regex>

ICAO::ICAO(Coordinates::Values const& other) : Coordinates{other}
{
}

std::optional<Coordinates::Values> ICAO::from(std::string_view const& in)
{
	struct Composition
	{
		Composition(
			std::string_view const& in,
			std::pmr::match_results<std::string_view::const_iterator> const& m,
			std::size_t nDegrees,
			std::size_t nMinutes,
			std::size_t nSeconds,
			std::size_t nDirection) :
			degrees{fromSubmatch(in, m, nDegrees, 360.0, [] { throw DegreesOutOfRange{}; })},
			minutes{fromSubmatch(in, m, nMinutes, 60.0, [] { throw MinutesOutOfRange{}; })},
			seconds{fromSubmatch(in, m, nSeconds, 60.0, [] { throw SecondsOutOfRange{}; })},
			direction{in[m.position(nDirection)]}
		{
		}
		double toDouble() const
		{
			double d{degrees + (minutes + seconds / 60.0) / 60.0};
			return direction == 'N' || direction == 'E' ? d : -d;
		}
		double degrees;
		double minutes;
		double seconds;
		char direction;
	};
	static const std::regex re{
		"[[:space:]]*([[:digit:]]{2})(([[:digit:]]{2})([[:digit:]]{2})?)?[[:space:]]*([NS])"
		"[[:space:]]*([[:digit:]]{3})(([[:digit:]]{2})([[:digit:]]{2})?)?[[:space:]]*([EW])[[:space:]]*"};
	std::optional<Coordinates::Values> result;
	std::pmr::match_results<std::string_view::const_iterator> m;
	if (std::regex_match(in.begin(), in.end(), m, re)) {
		result = Coordinates::Values{Composition{in, m, 1, 3, 4, 5}.toDouble(), Composition{in, m, 6, 8, 9, 10}.toDouble()};
		if (result.value().latitude < -90.0 || result.value().latitude > 90.0) {
			throw LatitudeOutOfRange{};
		}
		if (result.value().longitude < -180.0 || result.value().longitude > 180.0) {
			throw LongitudeOutOfRange{};
		}
	}
	return result;
}
