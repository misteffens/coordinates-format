//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "DegreesOutOfRange.h"

DegreesOutOfRange::DegreesOutOfRange() : std::range_error{"Degrees out of range."}
{
}
