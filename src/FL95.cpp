//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "FL95.h"
#include "DegreesOutOfRange.h"
#include "LatitudeOutOfRange.h"
#include "LongitudeOutOfRange.h"
#include "MinutesOutOfRange.h"
#include "format.h"
#include "fromSubmatch.h"
#include <charconv>
#include <cmath>
#include <regex>

FL95::FL95(Coordinates::Values const& other) : Coordinates{other}
{
}

std::optional<Coordinates::Values> FL95::from(std::string_view const& in)
{
	struct Composition
	{
		Composition(
			std::string_view const& in,
			std::pmr::match_results<std::string_view::const_iterator> const& m,
			std::size_t nDirection,
			std::size_t nDegrees,
			std::size_t nMinutes) :
			direction{in[m.position(nDirection)]},
			degrees{fromSubmatch(in, m, nDegrees, 360.0, [] { throw DegreesOutOfRange{}; })},
			minutes{fromSubmatch(in, m, nMinutes, 60.0, [] { throw MinutesOutOfRange{}; })}
		{
		}
		double toDouble() const
		{
			double d{degrees + minutes / 60.0};
			return direction == 'N' || direction == 'E' ? d : -d;
		}
		char direction;
		double degrees{0.0};
		double minutes{0.0};
	};
	static const std::regex re{
		"[[:space:]]*([NS])[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+(\\.[[:digit:]]*)?)"
		"[[:space:]]+([EW])[[:space:]]+([[:digit:]]+)[[:space:]]+([[:digit:]]+(\\.[[:digit:]]*)?)[[:space:]]*"};
	std::optional<Coordinates::Values> result;
	std::pmr::match_results<std::string_view::const_iterator> m;
	if (std::regex_match(in.begin(), in.end(), m, re)) {
		result = Coordinates::Values{Composition{in, m, 1, 2, 3}.toDouble(), Composition{in, m, 5, 6, 7}.toDouble()};
		if (result.value().latitude < -90.0 || result.value().latitude > 90.0) {
			throw LatitudeOutOfRange{};
		}
		if (result.value().longitude < -180.0 || result.value().longitude > 180.0) {
			throw LongitudeOutOfRange{};
		}
	}
	return result;
}

std::pmr::string FL95::toString() const
{
	struct Decomposition
	{
		Decomposition(char direction, double coordinate) : direction{direction}
		{
			degrees = static_cast<unsigned long>(std::round(std::abs(coordinate) * (60UL * 10UL)));
			minuteFraction = degrees % 10UL;
			degrees /= 10UL;
			minutes = degrees % 60UL;
			degrees /= 60UL;
		}
		char direction;
		unsigned long degrees{0};
		unsigned long minutes{0};
		unsigned long minuteFraction{0};
	};
	const Decomposition lat{latitude >= 0.0 ? 'N' : 'S', latitude};
	const Decomposition lon{longitude >= 0.0 ? 'E' : 'W', longitude};
	std::pmr::string result;
	std::format_to(
		std::back_inserter(result),
		"{} {:02} {:02}.{:01} {} {:03} {:02d}.{:01}",
		lat.direction,
		lat.degrees,
		lat.minutes,
		lat.minuteFraction,
		lon.direction,
		lon.degrees,
		lon.minutes,
		lon.minuteFraction);
	return result;
}
