//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef fromSubmatch_h_INCLUDED
#define fromSubmatch_h_INCLUDED

#include <functional>
#include <regex>
#include <string_view>

template<typename T>
T fromSubmatch(
	std::string_view const& in,
	std::pmr::match_results<std::string_view::const_iterator> const& m,
	std::size_t position,
	T maxValue,
	std::function<void()> outOfRangeF);

#endif // fromSubmatch_h_INCLUDED
