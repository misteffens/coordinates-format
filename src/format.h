//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef format_h_INCLUDED
#define format_h_INCLUDED

#include <format>
#include <iterator>
#include <memory_resource>
#include <string>

template<typename... Args>
std::pmr::string format(std::format_string<Args...> fmt, Args&&... args)
{
	std::pmr::string result;
	format_to(std::back_inserter(result), fmt, std::forward<Args>(args)...);
	return result;
}

#endif // format_h_INCLUDED
