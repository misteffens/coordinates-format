//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "UsageError.h"

UsageError::UsageError() :
	std::runtime_error{
		"Usage: coordinates-format <output-format> <input>"
		"\n\t<output-format> := decimal|dms|fl95|fpl|notam"
		"\nVersion: " COORDINATES_FORMAT_VERSION}
{
}
