//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "UsageError.h"
#include <Coordinates.h>
#include <iostream>

int main(int argc, char* argv[])
{
	int result{0};
	try {
		if (argc != 3) {
			throw UsageError{};
		}
		std::cout << Coordinates::create(argv[1], argv[2])->toString() << std::endl;
	} catch (std::exception const& e) {
		std::cerr << e.what() << std::endl;
		result = 1;
	}
	return result;
}
