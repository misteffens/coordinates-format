//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#include "UnknownInputFormat.h"

UnknownInputFormat::UnknownInputFormat() : std::range_error{"Unknown input format."}
{
}
