//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef LongitudeOutOfRange_h_INCLUDED
#define LongitudeOutOfRange_h_INCLUDED

#include <stdexcept>

class LongitudeOutOfRange : public std::range_error
{
public:
	LongitudeOutOfRange();
};

#endif // LongitudeOutOfRange_h_INCLUDED
