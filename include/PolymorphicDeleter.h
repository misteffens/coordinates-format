//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef PolymorphicDeleter_h_INCLUDED
#define PolymorphicDeleter_h_INCLUDED

#include <memory_resource>
#include <type_traits>

template<typename T>
class PolymorphicDeleter
{
public:
	constexpr PolymorphicDeleter(std::pmr::memory_resource* resource = std::pmr::get_default_resource()) noexcept :
		resource{resource}, bytes{sizeof(T)}, alignment{alignof(T)}
	{
	}

	template<typename U, typename = std::enable_if_t<std::is_convertible<U*, T*>::value>>
	constexpr PolymorphicDeleter(PolymorphicDeleter<U> const& other) noexcept :
		resource{other.getResource()}, bytes{other.getBytes()}, alignment{other.getAlignment()}
	{
	}

	constexpr void operator()(T* ptr)
	{
		resource->deallocate(ptr, bytes, alignment);
	}

	constexpr std::pmr::memory_resource* getResource() const
	{
		return resource;
	}

	constexpr std::size_t getBytes() const
	{
		return bytes;
	}

	constexpr std::size_t getAlignment() const
	{
		return alignment;
	}

private:
	std::pmr::memory_resource* resource;
	std::size_t bytes;
	std::size_t alignment;
};

#endif // PolymorphicDeleter_h_INCLUDED
