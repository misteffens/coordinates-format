//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef allocateUnique_h_INCLUDED
#define allocateUnique_h_INCLUDED

#include "PolymorphicDeleter.h"
#include <memory>
#include <type_traits>

template<typename T, typename... Args>
auto allocateUnique(std::pmr::polymorphic_allocator<T> const& alloc, Args&&... args) -> std::unique_ptr<T, PolymorphicDeleter<T>>
{
	std::pmr::polymorphic_allocator<T> a{alloc};
	T* ptr = std::allocator_traits<std::pmr::polymorphic_allocator<T>>::allocate(a, 1);
	try {
		std::allocator_traits<std::pmr::polymorphic_allocator<T>>::construct(a, ptr, std::forward<Args>(args)...);
		return std::unique_ptr<T, PolymorphicDeleter<T>>{ptr, PolymorphicDeleter<T>{a.resource()}};
	} catch (...) {
		std::allocator_traits<std::pmr::polymorphic_allocator<T>>::deallocate(a, ptr, 1);
		throw;
	}
}

#endif // allocateUnique_h_INCLUDED
