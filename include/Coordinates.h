//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef Coordinates_h_INCLUDED
#define Coordinates_h_INCLUDED

#include "PolymorphicDeleter.h"
#include <memory>
#include <string>
#include <string_view>

template<typename T>
class PolymorphicDeleter;

class Coordinates
{
public:
	using UniquePtr = std::unique_ptr<Coordinates, PolymorphicDeleter<Coordinates>>;

	struct Values
	{
		double latitude;
		double longitude;
	};

	virtual ~Coordinates() = default;
	static UniquePtr create(std::string_view const& formatId, std::string_view const& in);
	virtual std::pmr::string toString() const = 0;

protected:
	Coordinates(Values const& other);

	double latitude;
	double longitude;
};

#endif // Coordinates_h_INCLUDED
