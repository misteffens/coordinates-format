//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef UnknownInputFormat_h_INCLUDED
#define UnknownInputFormat_h_INCLUDED

#include <stdexcept>

class UnknownInputFormat : public std::range_error
{
public:
	UnknownInputFormat();
};

#endif // UnknownInputFormat_h_INCLUDED
