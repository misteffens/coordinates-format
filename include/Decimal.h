//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef Decimal_h_INCLUDED
#define Decimal_h_INCLUDED

#include "Coordinates.h"
#include <optional>
#include <string_view>

class Decimal : public Coordinates
{
public:
	Decimal(Coordinates::Values const& other);
	static std::optional<Coordinates::Values> from(std::string_view const& in);
	std::pmr::string toString() const override;
};

#endif // Decimal_h_INCLUDED
