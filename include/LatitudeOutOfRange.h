//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef LatitudeOutOfRange_h_INCLUDED
#define LatitudeOutOfRange_h_INCLUDED

#include <stdexcept>

class LatitudeOutOfRange : public std::range_error
{
public:
	LatitudeOutOfRange();
};

#endif // LatitudeOutOfRange_h_INCLUDED
