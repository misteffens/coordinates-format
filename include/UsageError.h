//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef UsageError_h_INCLUDED
#define UsageError_h_INCLUDED

#include <stdexcept>

class UsageError : public std::runtime_error
{
public:
	UsageError();
};

#endif // UsageError_h_INCLUDED
