//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef UnknownOutputFormat_h_INCLUDED
#define UnknownOutputFormat_h_INCLUDED

#include <stdexcept>

class UnknownOutputFormat : public std::range_error
{
public:
	UnknownOutputFormat();
};

#endif // UnknownOutputFormat_h_INCLUDED
