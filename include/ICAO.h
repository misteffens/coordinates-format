//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef ICAO_h_INCLUDED
#define ICAO_h_INCLUDED

#include "Coordinates.h"
#include <optional>
#include <string_view>

class ICAO : public Coordinates
{
public:
	ICAO(Coordinates::Values const& other);
	static std::optional<Coordinates::Values> from(std::string_view const& in);
};

#endif // ICAO_h_INCLUDED
