//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef FPL_h_INCLUDED
#define FPL_h_INCLUDED

#include "Coordinates.h"
#include "ICAO.h"

class FPL : public ICAO
{
public:
	FPL(Coordinates::Values const& other);
	std::pmr::string toString() const override;
};

#endif // FPL_h_INCLUDED
