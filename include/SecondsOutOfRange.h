//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef SecondsOutOfRange_h_INCLUDED
#define SecondsOutOfRange_h_INCLUDED

#include <stdexcept>

class SecondsOutOfRange : public std::range_error
{
public:
	SecondsOutOfRange();
};

#endif // SecondsOutOfRange_h_INCLUDED
