//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef FL95_h_INCLUDED
#define FL95_h_INCLUDED

#include "Coordinates.h"
#include <optional>
#include <string_view>

class FL95 : public Coordinates
{
public:
	FL95(Coordinates::Values const& other);
	static std::optional<Coordinates::Values> from(std::string_view const& in);
	std::pmr::string toString() const override;
};

#endif // FL95_h_INCLUDED
