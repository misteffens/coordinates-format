//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef NOTAM_h_INCLUDED
#define NOTAM_h_INCLUDED

#include "Coordinates.h"
#include "ICAO.h"

class NOTAM : public ICAO
{
public:
	NOTAM(Coordinates::Values const& other);
	std::pmr::string toString() const override;
};

#endif // NOTAM_h_INCLUDED
