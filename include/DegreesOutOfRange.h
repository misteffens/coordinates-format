//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef DegreesOutOfRange_h_INCLUDED
#define DegreesOutOfRange_h_INCLUDED

#include <stdexcept>

class DegreesOutOfRange : public std::range_error
{
public:
	DegreesOutOfRange();
};

#endif // DegreesOutOfRange_h_INCLUDED
