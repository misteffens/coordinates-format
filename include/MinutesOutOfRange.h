//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     GPL-3.0-or-later
//

#ifndef MinutesOutOfRange_h_INCLUDED
#define MinutesOutOfRange_h_INCLUDED

#include <stdexcept>

class MinutesOutOfRange : public std::range_error
{
public:
	MinutesOutOfRange();
};

#endif // MinutesOutOfRange_h_INCLUDED
